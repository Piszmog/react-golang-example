package main

import (
    "net/http"
    "github.com/julienschmidt/httprouter"
    "log"
    "github.com/Piszmog/golang-react-server-example/db"
    "github.com/Piszmog/golang-react-server-example/webrouter"
    "github.com/rs/cors"
)

func main() {
    var connectionService db.ConnectionService
    connectionService.Connect("localhost:8080", "test")
    defer connectionService.Close()
    router := httprouter.New()
    webrouter.SetupMovieRoutes(router, &connectionService)
    handler := cors.Default().Handler(router)
    log.Fatal(http.ListenAndServe(":8080", handler))
}
