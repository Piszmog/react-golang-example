module github.com/Piszmog/golang-react-server-example

require (
	github.com/globalsign/mgo v0.0.0-20180424091348-efe0945164a7
	github.com/google/uuid v0.0.0-20171129191014-dec09d789f3d
	github.com/julienschmidt/httprouter v0.0.0-20180411154501-adbc77eec0d9
	github.com/pkg/errors v0.8.0
	github.com/rs/cors v1.4.0
	github.com/stretchr/testify v1.2.2
	go.uber.org/zap v1.8.0
	gopkg.in/check.v1 v1.0.0-20161208181325-20d25e280405
)
