package db

import (
    "github.com/globalsign/mgo"
    "github.com/globalsign/mgo/bson"
    "go.uber.org/zap"
    "github.com/Piszmog/golang-react-server-example/util"
    "github.com/Piszmog/golang-react-server-example/model"
)

type ConnectionService struct {
    server   string
    database string
    session  *mgo.Session
}

const (
    COLLECTION = "movies"
)

var logger zap.SugaredLogger

// Creates the logger for the package
func init() {
    zapLogger := util.CreateLogger()
    defer zapLogger.Sync()
    logger = *zapLogger.Sugar()
}

// Connect to the Mongdb Database using the server name and the database name
func (m *ConnectionService) Connect(server string, database string) {
    session, err := mgo.Dial(m.server)
    if err != nil {
        logger.Fatalf("failed to connect to the db, %v", err)
    }
    m.session = session
    m.server = server
    m.database = database
}

// Connect to the Mongodb Database using a URI connection string
func (m *ConnectionService) ConnectWithURL(url string) {
    info, parseErr := mgo.ParseURL(url)
    if parseErr != nil {
        logger.Fatalf("failed to parse URL, %v", parseErr)
    }
    session, err := mgo.DialWithInfo(info)
    if err != nil {
        logger.Fatalf("failed to connect to the db, %v", err)
    }
    m.session = session
    m.database = info.Database
    m.session = session
}

// Finds all movies
func (m *ConnectionService) FindAll() ([]model.Movie, error) {
    var movies []model.Movie
    session := m.session.Copy()
    defer session.Close()
    err := session.DB(m.database).C(COLLECTION).Find(bson.M{}).All(&movies)
    return movies, err
}

// Finds the movie matching the provided id
func (m *ConnectionService) FindById(id string) (model.Movie, error) {
    var movie model.Movie
    session := m.session.Copy()
    defer session.Close()
    query := session.DB(m.database).C(COLLECTION).FindId(id)
    if query == nil {
        return movie, nil
    }
    err := query.One(&movie)
    return movie, err
}

// Inserts the provided movie
func (m *ConnectionService) Insert(movie model.Movie) error {
    session := m.session.Copy()
    defer session.Close()
    err := session.DB(m.database).C(COLLECTION).Insert(&movie)
    return err
}

// Delete the movie matching the id
func (m *ConnectionService) Delete(id string) error {
    session := m.session.Copy()
    defer session.Close()
    err := session.DB(m.database).C(COLLECTION).RemoveId(id)
    return err
}

// Updates the movie matching the id with the provided body
func (m *ConnectionService) Update(id string, movie model.Movie) error {
    session := m.session.Copy()
    defer session.Close()
    err := session.DB(m.database).C(COLLECTION).UpdateId(id, &movie)
    return err
}

func (m *ConnectionService) Close() {
    m.session.Close()
}
